<%-- 
    Document   : login
    Created on : May 27, 2015, 11:43:02 PM
    Author     : guilherme
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" isErrorPage="false" %>

<jsp:useBean class="pratica.jsp.LoginBean" id="login" scope="session"/>
<jsp:setProperty name="login" property="*" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Login</title>
   </head>
   <body>
      <form method="post" action="/web-exemplo-jsp/login.jsp">
          Código: <input type="text" name="login" value="${login.login}"/><br/>
          Nome: <input type="password" name="senha" value="${login.senha}" /><br/>
         Perfil: <select name="perfil">
                     <option value="1">Cliente</option>
                     <option value="2">Gerente</option>
                     <option value="3">Administrador</option>
                 <select>
         <input type="submit" value="Enviar"/><br />
         <%
             if (request.getMethod().equals("POST")) {
                 if (login.getLogin().equals(login.getSenha()))
                 {
                     String perfil = "";
                     
                     switch (login.getPerfil()) {
                         case 1:
                             perfil = "Cliente";
                             break;
                         case 2:
                             perfil = "Gerente";
                             break;
                         case 3:
                             perfil = "Administrador";
                             break;
                         default:
                             break;
                     }
                     
                     out.println("<div style=\"color: blue;\">"
                             + perfil + ", login bem sucedido, para " + login.getLogin()
                             + "<div> ");
                 } else {
                     out.println("<div style=\"color: red; font-style: italic;\">"
                             + "Acesso negado"
                             + "</div>");
                 }
             }     
         %>
      </form>
   </body>
</html>
